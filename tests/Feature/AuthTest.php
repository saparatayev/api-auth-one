<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testLogin()
    {
        $user = User::factory()->createOne();
        $response = $this->post(
            '/api/login',
            [
                'email' => $user->email,
                'password' => 'password',
            ]
        );
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'success',
            'data' => [
                'token',
                'token_type',
            ]
        ]);
        
        \JWTAuth::setToken($response->json('data.token'))->checkOrFail();

    }
}
